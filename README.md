# Slave to the Wage

SttW is my personal blog. I write a lot of things about Internet, Culture, Academic, etc.

main site is https://blog.peamisp.info

## What I Use?

Main tools is

- [Hugo Static Site Generator](https://gohugo.io)
- [Theme Hello Friend NG](https://themes.gohugo.io/hugo-theme-hello-friend-ng/)
- I custom a lot of things, but default original theme is also awesome
- Deploy with [Netlify](https://netlify.com]