---
title: "About Me"
draft: false
date: 2019-05-05T19:45:21+07:00
---

[{{< figure src="/img/about.gif" caption="Phewww..." >}}](https://www.null66913.net/blog/cyberpunk-moments/)

I'm a beginner front-end developer, technology geek, and dumb anarcho-communist.

Writing is my hobby, also exploring in the world of knowledge. I created this blog for expressing my thoughts, the realm of chaos and maniac.

I try to write as much as I can, even my lazy brain won't let me do that. Thanks for wandering through my place. If you love my blog, please share it to other folks, I will appreciate your kindness. You can contact me via E-mail or Twitter.

## Style Guide

I'm so obsessed with CYBERPUNK theme, so my styling appears in the cyberpunk-ish way&mdash;neonism. I used pink and black to express the dystopia images&mdash;time of late capitalism combines with technology to exploit people.

## Theme

As I use Hugo static site generator as the main tool for this blog, I can choose a good theme from their [website](https://themes.gohugo.io/). At first time when I still hadn't known how to style existing themes, I can't do much about choosing themes that suite me well (indeed, Hugo has so much less themes compare with Jekylle).

Then I found a good designed [Hello Friend NG Theme](https://themes.gohugo.io/hugo-theme-hello-friend-ng/). Its default style is so simple, I modified it so much such as,

- Add gif image in my home page.
- Add published date.
- Add share buttons.
- Change colors.

The good things of default style are,

- It's fully responsive included collapsible menu on mobile devices.
- So minimal.
- It has dark/light mode. But I don't use the light mode anymore. It's hard to be styled.
- The logo (home button) looks so greattttt

{{< figure src="/img/home.png" caption="whoa!, $cd /home/" >}}

## Fonts

I use 4 fonts

- [Faster One](https://fonts.google.com/specimen/Faster+One) for only main title in home page. It looks so futuristic, suites my blog most.
- [Karla](https://fonts.google.com/specimen/Karla) looks so great in paragraph, but I use it only on home page and menu.
- [Bai Jamjuree](https://fonts.google.com/specimen/Bai+Jamjuree). It's really a hard time to pick main font for blog posts. I write mainly in Thai, and Thai fonts are not various as English. I ended up with Bai Jamjuree after trying several Thai fonts, this one also has well-design English variant.
- [Anonymous Pro](https://fonts.google.com/specimen/Anonymous+Pro) for codes. I think this modern monospace font is really beutiful.

## Colors

I use only 2 main colors

1. #fe5186 Pinky Winky Neon for accent color.
2. #222 for dark background.

## Are Hugo Themes hard to style?

Not really, but You should have basic SASS knowledge. It really depends on what you want to modify, for me it's just adding something, change something and take-out something. Anyway, I think that it's the good idea to build your website from scratch.But I don't want to focus on it much, I just want to write.

If you are interesting in Hugo styling, please contact me. I will help you as much as I can.
