---
title: "สร้าง Theme VS Code ในแบบของคุณเอง ด้วย Theme Generator Extension"
summary: "คุณเคยรู้สึกบ้างหรือเปล่าว่าอยากจะสร้าง Theme ใน VS Code ขึ้นมาเองเพื่อให้เหมาะสมกับความต้องการของคุณ วันนี้จะมาแนะนำส่วนเสริมที่ชื่อว่า Theme Generator ซึ่งเป็นตัวช่วยในการสร้าง Custom Theme ของคุณเองโดยที่ไม่ต้องใช้ความรู้ด้าน Programming ใดๆ"
date: 2019-05-14T11:10:21+07:00
draft: false
tags: ["VS Code", "Theme"]
categories: ["ETC"]
slug: "vs-code-theme-generator"
featured_image: "/img/vscode-theme/06.png"
---

ตั้งแต่ผมใช้ Visual Studio Code มาจนถึงตอนนี้ ผมก็ได้ปรับแต่ง เลือก Theme มาแล้วหลายตัว บางตัวก็ถูกใจใช้ยาวๆ บางตัวไม่ถูกใจเลยลบทิ้งไป ส่วนมากแล้วผมจะใช้ Dark Theme เป็นหลัก และไม่ต้องการ contrast ที่สูงเกินไป แต่สิ่งสำคัญเลย มันต้องเข้ากับหน้าตาของ desktop ผมได้ด้วย

### แล้วทำไมต้องเข้ากับหน้าตา desktop

คืองี้ครับ ผมใช้ Linux และ i3wm เป็น windows manager ซึ่งไอ้ตัว i3 เนี่ยมันปรับแต่งหน้าตาได้หลากหลายมากๆ ศัพท์ในวงการเขาเรียกว่า ricing ถ้าแปลให้เข้าใจง่ายๆ ก็คือการทำให้หน้าตา desktop ของคุณ รวมทั้ง environment ทั้งหมดสวยงามขึ้น น่าใช้ขึ้น หรือใครจะแต่งออกมาแปลกๆ ก็ไม่ว่ากัน (เพราะสุนทรียะของแต่ละคนก็ไม่เหมือนกันอ่ะนะ) ใครสนใจเรื่อง ricing ก็ลองเข้าไปดูใน reddit [r/unixporn](https://www.reddit.com/r/unixporn/) ได้ครับ ส่วนนี่คือหน้าตาของ desktop ผมในตอนนี้ (อนาคตคงเปลี่ยนอีกถ้าเบื่อแล้ว)

{{< figure src="/img/vscode-theme/01.png" caption="พยายามทำให้ออกมาเป็นแนว cyberpunk" >}}

จะเห็นได้ว่า color scheme ใน terminal (rxvt-unicode) นั้นผมพยายามใช้ชุดสีจาก wallpaper เพื่อให้มันดูเข้ากันมากที่สุด แน่นอนว่า color scheme ชุดนี้ก็ถูกนำไป apply กับโปรแกรมส่วนใหญ่ของผม เช่น file manager หรือ rofi launcher เป็นต้น

{{< figure src="/img/vscode-theme/02.png" caption="หน้าตา file manager" >}}

เอาล่ะ จะเห็นแล้วใช่ไหมว่าทำไมผมถึงต้อง generate VS code theme ขึ้นมาเอง เพราะว่ามันไม่มี theme ที่ตรงกับชุดสีที่ผมใช้อยู่เลยยังไงล่ะ! เอาแค่สีพื้นหลังก็ไม่มีแล้ว การสร้างขึ้นมาเองจึงเป็นทางเลือกสุดท้าย ผมก็เลยลองค้นคำว่า theme generator มันก็ขึ้นส่วนเสริมตัวนี้มาให้ครับ

{{< figure src="/img/vscode-theme/03.png" caption="ดูจากจำนวนดาวน์โหลดแล้วตีความได้เลยว่า ไม่ค่อยมีคนใช้แหะ" >}}

วิธีใช้ก็ไม่มีอะไรยุ่งยากเลยครับ คุณก็ติดตั้งส่วนเสริมตัวนี้ซะก่อน พอเสร็จแล้วก็เปิดไฟล์ *settings.json* ขึ้นมา หน้าตาอย่างนี้

{{< figure src="/img/vscode-theme/04.png" caption="settings.json" >}}

จากนั้นก็ก็อปโค้ดต่อไปนี้ไปวางในไฟล์ *settings.json*

~~~ json
"themeGenerator.colors": {
    "background": "#21282d",
    "foreground": "#fff",
    "primary": "#327193",
    "keywords": "#43baba",
    "functions": "#3fa5c1",
    "strings": "#b9ecfa",
    "numbers": "#8789c0",
    "punctuation": "#494f53",
    "operators": "#327193",
    "comment": "#3d4b54",
    "tags": "#43baba",
    "attributes": "#3fa5c1",
    "properties": "#43baba",
    "builtins": "#b5ca8d",
    "variables": "#fff",
    "types": "#3fa5c1"
}
~~~
  
ทีนี้ก็มาเปลี่ยนค่าสีต่างๆ ที่เราต้องการ ซึ่งหลักๆ ที่ผมใช้ก็คือ

- background (สีพื้นหลัง)
- foreground (สีอักษร)
- tags (สี html tags)
- attributes (สี html attributes)
- properties (สี css properties)
- numbers (สีของตัวเลข เช่น 20px, 3em, 40vw เป็นต้น)  
  
(ผมเขียน HTML กับ CSS เป็นหลักครับ)

พอเราตั้งค่าต่างๆ เสร็จสิ้น ก็มาถึงเวลา generate theme เพื่อใช้งานกันแล้ว มีขั้นตอนดังนี้ครับ

1. เปิด command palette ขึ้นมา (ctrl + shift +p)
2. เลือกคำสั่ง 'Generate Theme'
3. เมื่อ generate theme เสร็จแล้ว เราต้อง reload window ก่อน โดยใช้คำสั่ง 'Developer: Reload Window' ใน command palette
4. reload เสร็จแล้ว เราก็สามารถไปเลือก theme ใน theme chooser ได้เลยครับ ตัว theme จะมีชื่อว่า *GeneratedTheme*

{{< figure src="/img/vscode-theme/05.png" caption="เลือก GeneratedTheme" >}}

เท่านี้ก็เป็นอันเสร็จสิ้น จะสังเกตได้ว่าสีของ status bar รวมทั้ง UI ทั้งหมดก็เปลี่ยนด้วย (สี Status bar ให้ไปเซ็ตตรง primary ครับ)

{{< figure src="/img/vscode-theme/06.png" caption="Theme ที่ผมใช้ในปัจจุบันครับ" >}}

หวังว่าบทความสั้นๆ ชิ้นนี้จะมีประโยชน์ต่อคนที่อยากจะแต่ง theme ขึ้นมาเอง เพราะผมเชื่อว่าสภาพแวดล้อมที่ดีก็จะทำให้การทำงานมีความสุขไปด้วยครับ แต่ว่าถ้าไม่ซีเรียสอะไรมาก Theme สวยๆ ให้โหลดก็มีเยอะแยะครับ เลือกเอาตามใจชอบได้เลย โอกาสหน้าผมจะมาเล่าถึงการ ricing desktop ให้อ่านกัน แล้วคุณจะพบว่า เราสามารถทำให้ Linux environment สวยงามได้อย่างใจหวังเลยล่ะครับ